# I.Tauraitė_I.Balienienė_Projektas_007
 
 Spark darbinės aplinkos paruošimas - https://bitbucket.org/daraliu/ktu-mgmf-ubuntu-server-1604/src/master/ .
 Atsidarius git bash terminalo langą nusiklonuoti repozitoriją, vykdant komandą git clone https://ievatau@bitbucket.org/ievatau/i.tauraite_i.balieniene_projektas_007.git .
-Klonavimas atliekamas į darbinę serverio direktoriją.
+Klonavimas atliekamas į darbinę serverio direktoriją.
 Parsisiuntus programą aggregation.R ir duomenis customer_usage_0007.csv paleisti programą. 
 Output bus naujai sukurtas aplankas -  aggregated_usage_csv .
 Atsidarius clustering_kmeans.R programą, kurios input bus aggregated_usage_csv aplankas, įvykdyti programą.
 Gauti klasterizavimo rezultatai gali būti lyginami su customer_churn_0007.csv failu, kuriame 1 - vartotojas paliko kompaniją, o 0 - vartotojas nepaliko komapnijos.
 
 Prisegami PNG histogramų failai ir PDF projekto ataskaita.
 Kiekviena klasterį nusiskaitom i datafile ir paverčiam į CSV.
 Ištrinam Prediction ir Feature_columns ir feature stulpelius iš CSV failo.
 Per komandinę eilutę įvykdom best_parameter.R. Į terminalą rašom Rscript best_parameter.R ir json failą.
 Rezultatas pateikiamas csv faile summary. Jame yra nuo 1 iki 30 renkamasi geriausias best depth, po to renkamasi geriausias skačius num trees su best depth parametru. Atvaizduojamas accuracy.
 Pasileidžiam ant test imčių prediction_on_test.R. Rezultatas duoda accuracy ir kitas metrikas taip pat kaip sumaišymo matricą.
 